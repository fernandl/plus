FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:20191002

ADD etc/yum.repos.d/* /etc/yum.repos.d/

# Start from empty cache
RUN yum clean all && \
# Base packages
# To install man pages for different pkgs,
# yum must be configured without tsflags=nodocs
# which is set by default in the docker images.
    sed -i -e '/tsflags=nodocs/s/^/#/' /etc/yum.conf && \
    yum install -y \
    bash-completion \
    bc \
    jq \
    man \
    man-db \
    man-pages \
    openafs-krb5 \
    passwd \
    vim \
    python3-pip \
    yum-plugin-priorities && \
# Condor packages
    yum install -y \
    cernbatchsubmit \
    condor \
    condor-classads \
    condor-external-libs \
    condor-procd \
    condor-python \
    condor-std-universe \
    ngbauth-submit && \
# OpenStack packages
    yum install -y \
	--disableplugin=protectbase \
	python-barbicanclient \
	python-decorator \
	python-heatclient \
	python-ironic-inspector-client \
	python-keystoneclient-x509 \
	python-keystoneclient-kerberos \
	python-openstackclient \
	python-swiftclient \
	python2-cryptography \
	python2-ironicclient \
	python2-magnumclient \
	python2-manilaclient \
	python2-mistralclient \
# Kubernetes clients
	kubernetes-client \
	helm \
	argo && \
# OpenStack completion
	openstack complete > /usr/share/bash-completion/completions/openstack && \
# ai-tools
    yum install -y \
	ai-tools \
	wassh \
	wassh-ssm-cern && \
# Cleanup caches to reduce image size
    yum clean all && \
# Jupyter environment setup
    pip3 install --no-cache --upgrade pip && \
    pip3 install --no-cache jupyterhub==1.0.0 && \
    pip3 install --no-cache jupyterlab==1.1.4 && \
    pip3 install --no-cache notebook==6.0.1

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER} && passwd -d ${NB_USER}

WORKDIR ${HOME}
USER ${USER}

# Base configuration
ADD etc /etc
ADD jupyterlab-workspace.json start /

# User level data
ADD images ${HOME}/images
ADD Welcome.md ${HOME}/

RUN ln -s /eos ${HOME}/eos

ENTRYPOINT /start
